package com.hillel.itschool.list;

import org.junit.Test;
import static org.junit.Assert.*;

public class LinkedListTest {

    @Test
    public void testSizeEmptyList() {
        List list = new LinkedList();
        assertEquals(0, list.size());
    }

    @Test
    public void testSizeAfterInsert() {
        List<String> list = new LinkedList<String>();
        list.add("Element 1");
        assertEquals(1, list.size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveOutsideList() {
        List list = new LinkedList();
        list.remove(2);
    }

    @Test
    public void testGet() {
        List<String> list = new LinkedList<String>();
        list.add("Element 1");
        assertEquals("Element 1", list.get(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetOutsideList() {
        List<String> list = new LinkedList<String>();
        list.add("Element 1");
        list.get(3);
    }


    // TODO: Написать тесты для остальных сценариев использования методов класса LinkedList.

}
