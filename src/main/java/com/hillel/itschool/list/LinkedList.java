package com.hillel.itschool.list;

/**
 * Каркас реализации. Добавлен в качестве подсказки.
 */
public class LinkedList<T> implements List<T> {

    // Голова списка.
    Node<T> head = null;

    @Override
    public void add(T element) {
        // TODO: добавить реализацию
    }

    @Override
    public void add(T element, int index) {
        // TODO: добавить реализацию
    }

    @Override
    public T remove(int index) {
        // TODO: добавить реализацию
        return null;
    }

    @Override
    public T get(int index) {
        // TODO: добавить реализацию
        return null;
    }

    @Override
    public int size() {
        // TODO: добавить реализацию
        return 0;
    }

    /**
     * В этом классе инкапсулирована пара "значение -> ссылка на следующий элемент".
     * Такие объекты удобно использовать в самом списке, но пользователям
     * класса о его существовании знать не нужно. Поэтому private.
     */
    private static class Node<T> {
        public final T value;
        public final Node<T> next;

        public Node(T value, Node<T> next) {
            this.value = value;
            this.next = next;
        }
    }

}
